package com.usa.imuRecorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/*! Activity principal aplicación. Se utiliza para Ranging(Medir distancia)*/
/**
 * Author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
public class MainMenuActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        };

    /**
     * Solicitar el permiso ACCESS_COARSE_LOCATION (Localización aproximada) en tiempo de ejecución..
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Ejecuta algo cuando la aplicación se detiene
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void launchImuActivity(View view){
        Intent intento = new Intent(this, ImuActivity.class);
        startActivity(intento);
    }
}
