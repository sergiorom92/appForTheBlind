# Aplicación móvil para personas invidentes

Este es un repositorio para la administración del proyecto de la aplicación móvil para invidentes.

En este momento el repositorio cuenta con 6 directorios (cada directorio contiene una aplicación móvil independiente):

- hybridAppForTheBlind (Aplicación híbrida Apache Cordova con Ionic)
- appForTheBlind (Integración de todos los proyectos).
- beaconDetector (Aplicación para detección de balizas BLE).
- IMURecorder (Aplicación para monitoreo de IMU).
- mapDrawing (Aplicación para representación de mapa).
- TangoProyect (Implementación de Motion Tracking de Tango).
