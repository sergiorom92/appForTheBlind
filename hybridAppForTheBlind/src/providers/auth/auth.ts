/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class AuthProvider {
  constructor(public af: AngularFireAuth,
    public storage: Storage,
    public afs: AngularFirestore
  ) { }

  async signin(credentails) {
    let datosIngreso = await this.af.auth.signInWithEmailAndPassword(credentails.email, credentails.password);
    // Obtiene un token de FCM
    await this.storage.ready();
    await this.storage.set('uid', datosIngreso.user.uid);
    let user = await this.afs.collection('users').doc(datosIngreso.user.uid).ref.get();
    if (user.exists) {
      let storage = await this.storage.set('user_data', user.data());
      console.log('Success storing user', storage);
    }
    return;
  }

  isLoggedIn(callback = null): Promise<any> {
    return new Promise(resolve => {
      this.af.auth.onAuthStateChanged(res => {
        if (res) {
          this.storage.set('uid', res.uid);
          if (callback) {
            callback(true);
          }
          resolve(true);
        } else {
          if (callback) {
            callback(false);
          }
          resolve(false);
        }
      });
    });
  }

  createAccount(credentials) {
    return this.af.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
  };

  logout() {
    return this.af.auth.signOut();
  };

  forgotPassword(email) {
    return this.af.auth.sendPasswordResetEmail(email);
  };

  checkAccountAvailable(email) {
    return this.af.auth.fetchProvidersForEmail(email);
  }
}