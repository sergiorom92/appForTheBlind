/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
import { Injectable } from '@angular/core';
import { AlertController, ActionSheetController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class UtilProvider {
  loader: any;
  constructor(public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController) { }

  doAlert(title, message, buttonText) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [buttonText]
    });
    return alert;
  }
  showLoading(message = "Please wait...") {
    this.loader = this.loadingCtrl.create({
      content: message,
    });
    this.loader.present();
  }

  hideLoading() {
    this.loader.dismissAll();
  }
  showConfirmation(title: string, message: string, callback: any) {
    const confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          handler: () => { }
        },
        {
          text: 'Accept',
          handler: () => {
            callback();
          }
        }
      ]
    });
    confirm.present();
    return;
  }
}