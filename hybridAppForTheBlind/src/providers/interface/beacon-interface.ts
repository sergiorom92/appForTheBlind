import { Injectable } from '@angular/core';
/*
  Generated class for the FirebaseBeaconDatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export abstract class BeaconInterface {

  abstract insertBeacon(beacon: any): void;
  abstract getBeacon(beaconId:String): any;
}
