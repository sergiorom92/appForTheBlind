import { Injectable } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { IBeacon } from '@ionic-native/ibeacon';
/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Injectable()
export class IbeaconProvider {

  delegate: any;
  region: any;

  constructor(public platform: Platform, public events: Events, public iBeacon: IBeacon) {
  }

  initialise(): any {
    let promise = new Promise((resolve, reject) => {
      // we need to be running on a device
      if (this.platform.is('cordova')) {

        // Request permission to use location on iOS
        this.iBeacon.requestAlwaysAuthorization();

        // create a new delegate and register it with the native layer
        this.delegate = this.iBeacon.Delegate();

        // Subscribe to some of the delegate's event handlers
        this.delegate.didRangeBeaconsInRegion()
          .subscribe(
            data => {
              this.events.publish('didRangeBeaconsInRegion', data);
            },
            error => console.error()
          );

        this.delegate.didStartMonitoringForRegion()
          .subscribe(
            data => console.log('didStartMonitoringForRegion: ', data),
            error => console.error()
          );
        this.delegate.didEnterRegion()
          .subscribe(
            data => {
              console.log('didEnterRegion: ', data);
            }
          );

        // setup a beacon region – CHANGE THIS TO YOUR OWN UUID
        this.region = this.iBeacon.BeaconRegion('deskBeacon', 'b9407f30-f5f8-466e-aff9-25556b57fe6d');

        // start ranging
        this.iBeacon.startRangingBeaconsInRegion(this.region)
          .then(
            () => {
              resolve(true);
            },
            error => {
              console.error('Failed to begin monitoring: ', error);
              resolve(false);
            }
          );
      } else {
        console.error("This application needs to be running on a device");
        resolve(false);
      }
    });

    return promise;
  }
}