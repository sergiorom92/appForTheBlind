import { Injectable } from '@angular/core';
// plugins
import { Hotspot } from '@ionic-native/hotspot';

/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Injectable()
export class WifiProvider {

  constructor(private hotspot: Hotspot) {  }

  getWifiBeacons(){
    return this.hotspot.scanWifi();
  }

}
