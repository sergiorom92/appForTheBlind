import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BeaconInterface } from '../interface/beacon-interface'

/*
  Generated class for the FirebaseBeaconDatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseBeaconDatabaseProvider implements BeaconInterface {

  constructor() {
  }

  public insertBeacon(beacon:any) {
    console.log(beacon);
  }

  public getBeacon() {

  }

}
