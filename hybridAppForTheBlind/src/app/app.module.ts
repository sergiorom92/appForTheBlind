/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */

// Plugins
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MapDrawerPage } from '../pages/map/mapDrawer';
import { ImuPage } from '../pages/imu/imu';
import { BeaconConfigMenuPage } from '../pages/beacon-config-menu/beacon-config-menu';

// Providers
import { IbeaconProvider } from '../providers/ibeacon/ibeacon';
import { WifiProvider } from '../providers/wifi/wifi';
import { BeaconsPage } from '../pages/beacons/beacons';
import { Gyroscope } from '@ionic-native/gyroscope';
import { DeviceMotion } from '@ionic-native/device-motion';
import { IBeacon } from '@ionic-native/ibeacon';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Hotspot } from '@ionic-native/hotspot';
import { IonicStorageModule } from '@ionic/storage';
import { AuthProvider } from '../providers/auth/auth';
import { UtilProvider } from '../providers/util/util';

export const firebase = {
  apiKey: "AIzaSyCAOSV3vsbMnXWBJ6oAdQmdBnbeIxXy7PM",
  authDomain: "dev-guild-151307.firebaseapp.com",
  databaseURL: "https://dev-guild-151307.firebaseio.com",
  projectId: "dev-guild-151307",
  storageBucket: "dev-guild-151307.appspot.com",
  messagingSenderId: "1087475786663"
};



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MapDrawerPage,
    ImuPage,
    BeaconsPage,
    BeaconConfigMenuPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MapDrawerPage,
    ImuPage,
    BeaconsPage,
    BeaconConfigMenuPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Gyroscope,
    DeviceMotion,
    IBeacon,
    Hotspot,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    IbeaconProvider,
    WifiProvider,
    AuthProvider,
    UtilProvider
  ]
})
export class AppModule { }
