import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the BeaconConfigMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-beacon-config-menu',
  templateUrl: 'beacon-config-menu.html',
})
export class BeaconConfigMenuPage {

  period: number = 0;
  beaconKind: string = 'all';

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private events: Events) {
    this.init();
  }

  async init() {
    let period = await this.storage.get('beaconPeriod');
    if (!period) {
      period = 1000;
      await this.storage.set('beaconPeriod', period);
    }
    let kind = await this.storage.get('beaconKind');
    if (!kind) {
      kind = 'all';
      await this.storage.set('beaconKind', kind);
    }
    this.beaconKind = kind;
    this.period = period;
    return;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BeaconConfigMenuPage');
  }

  async save() {
    await this.storage.set('beaconPeriod', this.period);
    await this.storage.set('beaconKind', this.beaconKind);
    let config = {
      period: this.period,
      beaconKind: this.beaconKind
    }
    this.events.publish('updateBeaconConfig', config);
    this.navCtrl.pop();
    return;
  }

}
