/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider } from '../../providers/util/util';
import { validateEmail } from '../../validators/email';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home'



@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  homePage = HomePage;
  loginForm: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public auth: AuthProvider,
    public util: UtilProvider,
    public storage: Storage,
    public alertCtrl: AlertController) { }

  ionViewDidLoad() { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, validateEmail]),
      password: new FormControl("", [Validators.required, Validators.minLength(6)])
    });
  }

  async signin() {
    try {
      this.auth.signin(this.loginForm.value).then(async data => {
        await this.storage.ready();
        // await this.storage.set('uid', data.uid);
        this.navCtrl.setRoot(HomePage);
      }).catch(async error => {
        let alert = this.util.doAlert("Error", error.message, "Ok");
        alert.present();
      });
    } catch (error) {
      let alert = this.util.doAlert("Error", error.message, "Ok");
      alert.present();
    }
  }


  async sendRecoveryEmail(email) {
    let alert;
    try {
      await this.auth.forgotPassword(email)
      alert = this.util.doAlert("Success", "Check your email to recover your password", "Ok");
      alert.present();
    } catch (error) {
      alert = this.util.doAlert("Error", error.message, "Ok");
      alert.present();
    }
  }

  async forgotPassword() {
    let alert = await this.alertCtrl.create();
    alert.setTitle('Please Enter your E-mail');
    alert.addInput({
      type: 'input',
      label: 'e-mail',
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        this.sendRecoveryEmail(data[0]);
      }
    });
    alert.present();
  }

}