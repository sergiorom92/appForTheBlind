import { Component } from '@angular/core';
import { Platform, NavController, NavParams } from 'ionic-angular';
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';

/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Component({
  selector: 'page-imu',
  templateUrl: 'imu.html',
})
export class ImuPage {

  girX: any;
  girY: any;
  girZ: any;
  girAltX: any;
  girAltY: any;
  girAltZ: any;

  accX: any;
  accY: any;
  accZ: any;
  accAltX: any;
  accAltY: any;
  accAltZ: any;

  gyroscopeSetInterval: any;
  accelerometerObservable: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private gyroscope: Gyroscope,
    private platform: Platform,
    private deviceMotion: DeviceMotion) {
    let options: GyroscopeOptions = {
      frequency: 100
    };
    if (this.platform.is('cordova')) {
      if (this.gyroscope) {
        this.gyroscope.getCurrent(options)
          .then((orientation: GyroscopeOrientation) => {
            this.girX = orientation.x;
            this.girY = orientation.y;
            this.girZ = orientation.z;
          })
          .catch()

        this.gyroscopeSetInterval = setInterval(() => {
          this.gyroscope.getCurrent(options)
            .then((orientation: GyroscopeOrientation) => {
              this.girAltX = orientation.x;
              this.girAltY = orientation.y;
              this.girAltZ = orientation.z;
            })
            .catch()
        }, 500);



        // this.gyroscope.watch()
        //   .subscribe((orientation: GyroscopeOrientation) => {
        //     this.girAltX = orientation.x;
        //     this.girAltY = orientation.y;
        //     this.girAltZ = orientation.z;
        //     console.log('Watch', orientation);
        //     console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
        //   });
      }
      if (this.deviceMotion) {
        // Get the device current acceleration
        this.deviceMotion.getCurrentAcceleration().then(
          (acceleration: DeviceMotionAccelerationData) => {
            //console.log(acceleration);
            this.accX = acceleration.x;
            this.accY = acceleration.y;
            this.accZ = acceleration.z;
          },
          (error: any) => console.log(error)
        );

        // Watch device acceleration
        this.accelerometerObservable = this.deviceMotion.watchAcceleration({ frequency: 100 })
          .subscribe((acceleration: DeviceMotionAccelerationData) => {
            //console.log(acceleration);
            this.accAltX = acceleration.x;
            this.accAltY = acceleration.y;
            this.accAltZ = acceleration.z;
          });
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImuPage');
  }

  ionViewWillLeave() {
    this.accelerometerObservable.unsubscribe();
    clearInterval(this.gyroscopeSetInterval);
    return;
  }


}
