export class MapManager {
    private map: any;
    private labSize: any;

    //init map
    constructor(labSize: any) {
        this.map = [];
        this.labSize = labSize;
        this.initMap();
    }

    initMap() {
        for (let i = 0; i < this.labSize.y; i += 1) {
            this.map[i] = [];
            for (let j = 0; j < this.labSize.x; j += 1) {
                this.map[i][j] = 0;
            }
        }

        //# create the grid
        for (let x = 0; x < this.labSize.x; x += 1) {
            for (let y = 0; y < this.labSize.y; y += 1) {
                this.map[y][x] = 0;
            }
        }
        for (let y = 0; y < this.labSize.y; y += 1) {
            for (let x = 0; x < this.labSize.x; x += 1) {
                this.map[y][x] = 0;
            }
        }
    }

    getMap() {
        return this.map;
    }


}
