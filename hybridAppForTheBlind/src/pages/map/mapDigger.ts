import { Map } from './map';

export class Mapdigger {

    private labyrinth: Map;
    private path: any;
    private directions: any[];
    private color: string;
    private size: number;
    private current: any;
    private pathColor: string;
    private target: any;

    constructor(labyrinth: Map, startX: number, startY: number, diggerSize: number, pathColor: string, diggerColor: string) {
        this.labyrinth = labyrinth;

        //dig the labyrinth
        this.labyrinth.generateMap();

        //get the dig path
        // Here algorythm must return path from one spot to another.
        this.path = this.labyrinth.generateSolution(startX,startY);
        this.path.push({ x: startX, y: startY });

        //set directions
        this.directions = [];
        for (var i = 0; i < this.path.length - 1; i += 1) {
            this.directions.push({
                x: this.path[i].x - this.path[i + 1].x,
                y: this.path[i].y - this.path[i + 1].y
            })
        }
        this.path.pop();

        //init properties
        this.color = diggerColor;
        this.size = Math.floor(diggerSize / 2);
        this.pathColor = pathColor;

        //init position holders
        this.current = { x: startX, y: startY };
        this.current.x = this.current.x * this.labyrinth.getCellSize().x +
            Math.floor((this.labyrinth.getCellSize().x + this.size) / 2);
        this.current.y = this.current.y * this.labyrinth.getCellSize().y +
            Math.floor((this.labyrinth.getCellSize().y + this.size) / 2);

        this.target = { x: 0, y: 0, d: { x: 0, y: 0 } };
        this.getNextTarget();

        this.drawDigger(this.color);

        return this;
    };

    drawDigger(color: string) {
        this.labyrinth.getCanvas().beginPath();
        this.labyrinth.getCanvas().arc(this.current.x, this.current.y, this.size, 0, Math.PI * 2);
        this.labyrinth.getCanvas().fillStyle = color;
        this.labyrinth.getCanvas().fill();
        return;
    }

    getNextTarget(clearFunc = null) {
        //if more targets exists
        if (this.path.length != 0) {
            this.target = this.path.pop();

            this.target.x = this.target.x * this.labyrinth.getCellSize().x +
                Math.floor((this.labyrinth.getCellSize().x + this.size) / 2);
            this.target.y = this.target.y * this.labyrinth.getCellSize().y +
                Math.floor((this.labyrinth.getCellSize().y + this.size) / 2);
            this.target.d = this.directions.pop();
            //otherwise stop the animation
        } else {
            if (clearFunc) {
                clearFunc();
            }
        }
        return;
    }

    moveDigger(digger: Mapdigger, clearFunc: Function) {
        //if target not reached move to next position
        if ((digger.current.x != digger.target.x) ||
            (digger.current.y != digger.target.y)) {
            //digger.drawDigger( 'white' );
            digger.drawDigger(this.pathColor);
            digger.current.x += digger.target.d.x;
            digger.current.y += digger.target.d.y;
            digger.drawDigger(digger.color);
            //otherwise get next target
        } else {
            digger.getNextTarget(clearFunc);
        }
    }
}