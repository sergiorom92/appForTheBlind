import { Map } from "./map";

export class Solver {
    private goDir: any;
    private grid: Map;
    private stack: any;
    private path: any[];
    private N: number = 1;
    private S: number = 2;
    private E: number = 4;
    private W: number = 8;
    private noTransitable : any;


    constructor(grid: Map) {
        this.grid = grid;
        //init path
        this.path = [];
        //init stack
        this.stack = [];


        //# dictionary with directions translated to digging moves
        this.goDir = [];
        this.goDir[this.N] = { x: 0, y: -1 };
        this.goDir[this.S] = { x: 0, y: 1 };
        this.goDir[this.E] = { x: 1, y: 0 };
        this.goDir[this.W] = { x: -1, y: 0 };

    }

    solve(startX, startY) {
        this.markUntransitable();
        return this.recursiveBacktrack(startX, startY);
    }

    markUntransitable(){
        this.noTransitable.forEach(spot=>{
            this.grid.getMap()[spot.posy][spot.posx] = 8;
        });
    }

    setNoTransitable(noTransitable:any){
        this.noTransitable = noTransitable;
    }

    recursiveBacktrack(x, y) {

        function allVisited(lab) {
            let cnt = lab.getLabSize().x * lab.getLabSize().y * 8;
            for (let y = 0; y < lab.getLabSize().y; y += 1) {
                for (let x = 0; x < lab.getLabSize().x; x += 1) {
                    cnt -= lab.getMap()[y][x];
                }
            }
            return cnt == 0;
        }

        //Implementation of the Recursive backtrack algorythm found at
        //https://en.wikipedia.org/wiki/Maze_generation_algorithm

        //Make the initial cell the current cell and mark it as visited
        this.grid.getMap()[y][x] = 8;

        //If the current cell has any neighbours which have not been visited
        let neighbours = [];
        let dirs = [this.N, this.E, this.S, this.W];
        for (let d = 0; d < dirs.length; d += 1) {
            let newX = x + this.goDir[dirs[d]].x;
            let newY = y + this.goDir[dirs[d]].y;

            if ((0 <= newX) && (newX < this.grid.getLabSize().x) &&
                (0 <= newY) && (newY < this.grid.getLabSize().y) &&
                (this.grid.getMap()[newY][newX] == 0)) {
                neighbours.push({ x: newX, y: newY, d: this.goDir[dirs[d]] })
            }
        }

        //While there are unvisited cells
        if (!allVisited(this.grid)) {
            let next = {
                x: null, y: null, d: {
                    x: null,
                    y: null
                }
            };
            if (neighbours.length) {
                //Choose randomly one of the unvisited neighbours
                next = neighbours[Math.floor(Math.random() * neighbours.length)];
                //Remove the wall between the current cell and the chosen cell
                this.grid.getMap()[y + next.d.y][x + next.d.x] = 0;
                //Push the current cell to the stack
                this.stack.push({ x: x, y: y });
                //record the path
                this.path.push({ x: next.x, y: next.y });
                if (next.x == this.grid.getDestination().x && next.y == this.grid.getDestination().y) {
                    return this.path;
                }
                //Make the chosen cell the current cell
                return this.recursiveBacktrack(next.x, next.y);
            } else {
                //Else if stack is not empty
                if (this.stack.length) {
                    //Pop a cell from the stack
                    next = this.stack.pop();
                    //record the path
                    this.path.push(next);
                    if (next.x == this.grid.getDestination().x && next.y == this.grid.getDestination().y) {
                        return this.path;
                    }
                    //Make it the current cell
                    return this.recursiveBacktrack(next.x, next.y);
                }
            }
        }
        return this.path;
    }

}