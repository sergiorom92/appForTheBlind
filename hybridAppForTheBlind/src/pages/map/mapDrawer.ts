import { Component } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import { Map } from './map';
import { Mapdigger } from './mapDigger';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { BeaconModel } from '../../models/beacon-model';
import { IbeaconProvider } from '../../providers/ibeacon/ibeacon';



/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Component({
  selector: 'page-map',
  templateUrl: 'mapDrawer.html',
})
export class MapDrawerPage {

  private labx: number = 10;
  private laby: number = 20;
  private cellX: number = 20;
  private cellY: number = 20;
  private destX: number = 5;
  private destY: number = 4;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private afs: AngularFirestore,
    public events: Events,
    private platform: Platform,
    private beaconProvider: IbeaconProvider
  ) {
  }

  documentDigger: Mapdigger;
  documentLabyrinth: Map;



  async btnClick() {
    let beaconMac = await this.checkClosestBeaconMac();
    let ref = this.afs.collection('maps').ref.where('beacons', 'array-contains', beaconMac);
    let data = (await ref.get()).docs;

    console.log('1', data[0].data());
    let closestMap = data[0].data();
    this.labx = closestMap.sizeX;
    this.laby = closestMap.sizeY;

    if (this.documentDigger) {
      delete (this.documentDigger);
    }
    var labX = this.labx;
    var labY = this.laby;
    var cellX = this.cellX;
    var cellY = this.cellY;

    document.getElementById('resultContainer').style.display = 'block';
    document.getElementById('instructions').innerHTML = 'Seleccione una posición en la grilla'
    document.getElementById('canvasContainer').style.width = labX * cellX + 2 + 'px';
    document.getElementById('canvasContainer').style.height = labY * cellY + 2 + 'px';

    this.documentLabyrinth = new Map(labX | 0, labY | 0, cellX | 0, cellY | 0,
      document.getElementById('labyrinthCanvas'), 1, 'black');
    this.documentLabyrinth.setDestination(this.destX, this.destY);
    this.documentLabyrinth.setNonTransitable(closestMap.nonTransitable);
    this.documentLabyrinth.drawLabyrinth(false);
    return;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

  canvasClick(event) {
    if (!this.documentDigger) {
      //get starting position
      var rect = document.getElementById('labyrinthCanvas').getBoundingClientRect();
      var start = this.documentLabyrinth.getCoordinates(event.clientX - rect.left, event.clientY - rect.top);

      document.getElementById('instructions').innerHTML = 'Buscando desde la posición' +
        ' x: ' + start.x + ', y: ' + start.y;
      //start digging
      this.documentDigger = new Mapdigger(this.documentLabyrinth, start.x, start.y, 6, '#06998f', 'red');
      var animate = window.setInterval(function () {
        this.documentDigger.moveDigger(this.documentDigger, clearFunc)
      }.bind(this), 0.5);
      var clearFunc = function () {
        window.clearInterval(animate);
        document.getElementById('instructions').innerHTML = 'Finalizado'
      }
    }
  }

  checkClosestBeaconMac() {
    let largestTx;
    let returnMac;
    return new Promise((resolve, reject) => {

      //resolve('F7:05:11:CA:E3:EF');
      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
          this.beaconProvider.initialise().then((isInitialised) => {
            if (isInitialised) {
              this.events.subscribe('didRangeBeaconsInRegion', (data) => {
                // update the UI with the beacon list
                let beaconList = data.beacons;
                beaconList.forEach((beacon) => {
                  console.log('beacon', beacon);
                  let id = this.afs.createId();
                  let beaconObject = new BeaconModel(beacon);
                  if (!largestTx) {
                    largestTx = beaconObject.rssi;
                    returnMac = 'F7:05:11:CA:E3:EF';
                  }
                });
                resolve(returnMac);
              });
            }
          });
        });
      } else {
        resolve('F7:05:11:CA:E3:EF');
      }

    });
  }

  ionViewWillLeave() {
    this.events.unsubscribe('didRangeBeaconsInRegion');
    return;
  }
}
