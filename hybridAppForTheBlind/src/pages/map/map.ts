import { Solver } from './solver';
import { MapManager } from './mapManager';
export class Map {

    private canvas: any;
    private size: any;
    private cellSize: any;
    private color: any;
    private destX: number;
    private destY: number;

    private mapManager: MapManager;
    private labyrinthSolver: Solver;
    private noTransitable : any[] ;


    getCellSize() {
        return this.cellSize;
    }

    getCanvas() {
        return this.canvas;
    }

    constructor(sizeX, sizeY, cellWidth, cellHeight, canvas, lineWidth, color) {
        this.init(sizeX, sizeY, cellWidth, cellHeight, canvas, lineWidth, color);
    }

    init(sizeX, sizeY, cellWidth, cellHeight, canvas, lineWidth, color) {
        this.labyrinthSolver = new Solver(this);
        this.canvas = {};
        //get canvas, set and clear it
        this.canvas = canvas.getContext("2d");
        this.canvas.clearRect(0, 0, this.canvas.width + 1, this.canvas.height + 1);
        canvas.width = sizeX * cellWidth + 2;
        canvas.height = sizeY * cellHeight + 2;

        this.canvas.lineWidth = lineWidth;
        this.canvas.strokeStyle = color.toString();

        //init size of the labyrinth
        this.size = {};
        this.size.x = sizeX;
        this.size.y = sizeY;

        //init cellSize;
        this.cellSize = {};
        this.cellSize.x = cellWidth;
        this.cellSize.y = cellHeight;

    }

    getMap() {
        return this.mapManager.getMap();
    }

    drawLabyrinth(removeWalls) {
        this.generateMap();
        //clear canvas
        this.canvas.clearRect(0, 0, this.canvas.width + 1, this.canvas.height + 1);
        //draw all cells
        for (let x = 0; x < this.getLabSize().x; x += 1) {
            for (let y = 0; y < this.getLabSize().y; y += 1) {
                this.drawCell(x, y, removeWalls);
            }
        }
        this.drawNoTransitable();
    }

    drawCell(x, y, removeWalls) {
        let canvas = this.canvas;
        canvas.strokeStyle = this.color;

        let line = function (x1, y1, x2, y2, dx, dy) {
            canvas.beginPath();
            canvas.moveTo(x1 * dx + 1, y1 * dy + 1);
            canvas.lineTo(x2 * dx + 1, y2 * dy + 1);
            canvas.stroke();
        }

        /*if (x1 == 3 && y1 == 4) {
            canvas.arc(x1 * dx + 2, y1 * dy + 2, 10, 0, 2 * Math.PI);
        }*/
        let cx = x * 2 + 1;
        let cy = y * 2 + 1;

        //North
        if (!removeWalls || (this.getMap()[cy - 1][cx] == 1)) {
            line(x, y, x + 1, y, this.cellSize.x, this.cellSize.y);
        }
        //East
        if (!removeWalls || (this.getMap()[cy][cx + 1] == 1)) {
            line(x + 1, y, x + 1, y + 1, this.cellSize.x, this.cellSize.y);
        }
        //South
        if (!removeWalls || (this.getMap()[cy + 1][cx] == 1)) {
            line(x + 1, y + 1, x, y + 1, this.cellSize.x, this.cellSize.y);
        }
        //West
        if (!removeWalls || (this.getMap()[cy][cx - 1] == 1)) {
            line(x, y + 1, x, y, this.cellSize.x, this.cellSize.y);
        }
    }

    drawNoTransitable() {
        let canvas = this.canvas;
        this.noTransitable.forEach(spot => {
            canvas.beginPath();
            canvas.fillRect((spot.posx) * this.cellSize.x, (spot.posy) * this.cellSize.y, this.cellSize.x, this.cellSize.y)
            //canvas.arc((x + 0.5) * this.cellSize.x, (y + 0.5) * this.cellSize.y + 1, 10, 0, 2 * Math.PI);
            canvas.stroke();

        })
    }

    generateMap() {
        this.mapManager = new MapManager(this.size);
        //clear the map
        this.mapManager.initMap();
        //clear the visited markers
        for (let x = 0; x < this.getLabSize().x; x += 1) {
            for (let y = 0; y < this.getLabSize().y; y += 1) {
                this.getMap()[y][x] = 0;
            }
        }
    }

    getCoordinates(x, y) {
        return {
            x: Math.floor(x / this.cellSize.x),
            y: Math.floor(y / this.cellSize.y)
        };
    }

    generateSolution(startX, startY) {
        this.labyrinthSolver.setNoTransitable(this.noTransitable)
        return this.labyrinthSolver.solve(startX, startY).slice().reverse();
    }

    getLabSize() {
        return this.size;
    }

    setDestination(destX: number, destY: number) {
        this.destX = destX;
        this.destY = destY;
        return;
    }

    getDestination() {
        return { x: this.destX, y: this.destY };
    }

    setNonTransitable(noTransitable){
        this.noTransitable = noTransitable;
        return;
    }

}

