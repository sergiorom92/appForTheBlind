// core stuff
import { Component } from '@angular/core';
import { NavController, Platform, Events } from 'ionic-angular';
import { NgZone } from '@angular/core';

// plugins
import { HotspotNetwork } from '@ionic-native/hotspot';

// providers
import { IbeaconProvider } from '../../providers/ibeacon/ibeacon';
import { WifiProvider } from '../../providers/wifi/wifi'
import { Storage } from '@ionic/storage';

// models
import { BeaconModel } from '../../models/beacon-model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { BeaconConfigMenuPage } from '../beacon-config-menu/beacon-config-menu';

import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';


/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Component({
  selector: 'page-beacons',
  templateUrl: 'beacons.html',
})
export class BeaconsPage {

  beacons: BeaconModel[] = [];
  beaconsEddystone: BeaconModel[] = [];
  zone: any;
  networks: any;
  beaconTime: number;
  beaconKind: string;
  lastSnapshotTime: number = 0;
  wifiInterval: any;

  beaconsObservable: any;

  constructor(public navCtrl: NavController,
    private gyroscope: Gyroscope,
    private platform: Platform,
    private deviceMotion: DeviceMotion,
    public beaconProvider: IbeaconProvider,
    public events: Events,
    public afs: AngularFirestore,
    private storage: Storage,
    public wifiProvider: WifiProvider) {
    // Load Settings
    this.loadSettings();
    // required for UI update
    this.zone = new NgZone({ enableLongStackTrace: false });

  }

  async loadSettings() {
    let period = await this.storage.get('beaconPeriod');
    if (!period) {
      period = 1000;
      await this.storage.set('beaconPeriod', period);
    }
    let kind = await this.storage.get('beaconKind');
    if (!kind) {
      kind = 'all';
      await this.storage.set('beaconKind', kind);
    }
    this.beaconKind = kind;
    this.beaconTime = period;

    this.events.subscribe('updateBeaconConfig', (data) => {
      this.beaconTime = data.period;
      this.beaconKind = data.beaconKind;
      if (this.platform.is('cordova')) {

        this.listenToWifiBeaconEvents();
      }
    });
    if (this.platform.is('cordova')) {

      this.listenToWifiBeaconEvents();
    }
    return;
  }

  ionViewDidLoad() {
    let reference = this.afs.collection('beacons').doc('ibeacon').collection('beacons');
    if (this.platform.is('cordova')) {
      this.listenToWifiBeaconEvents();
      this.initializeIBeacon();
    }
  }

  initializeIBeacon() {
    this.platform.ready().then(() => {
      this.beaconProvider.initialise().then((isInitialised) => {
        if (isInitialised) {
          this.listenToIBeaconEvents();
        }
      });
    });
  }

  listenToIBeaconEvents() {
    let options: GyroscopeOptions = {
      frequency: 100
    };
    this.beaconsObservable = this.events.subscribe('didRangeBeaconsInRegion', (data) => {
      let tiempo = new Date().getTime() - this.lastSnapshotTime;
      if (this.lastSnapshotTime == 0 || (tiempo) > this.beaconTime) {
        this.lastSnapshotTime = new Date().getTime();
        // update the UI with the beacon list
        this.zone.run(() => {
          this.beacons = [];
          let beaconList = data.beacons;
          if (this.platform.is('cordova')) {
            let gyroscope: any = {};
            let accelerometer: any = {};
            if (this.gyroscope) {
              this.gyroscope.getCurrent(options)
                .then((orientation: GyroscopeOrientation) => {
                  gyroscope.x = orientation.x;
                  gyroscope.y = orientation.y;
                  gyroscope.z = orientation.z;
                  if (this.deviceMotion) {
                    // Get the device current acceleration
                    this.deviceMotion.getCurrentAcceleration().then(
                      (acceleration: DeviceMotionAccelerationData) => {
                        //console.log(acceleration);
                        accelerometer.X = acceleration.x;
                        accelerometer.Y = acceleration.y;
                        accelerometer.Z = acceleration.z;
                        beaconList.forEach((beacon) => {
                          let id = this.afs.createId();
                          let reference = this.afs.collection('beacons').doc('ibeacon').collection('beacons');
                          let beaconObject = new BeaconModel(beacon);
                          this.beacons.push(beaconObject);
                          beaconObject.beacon.time = new Date().getTime();
                          beaconObject['accelerometer'] = accelerometer;
                          beaconObject['gyroscope'] = gyroscope;
                          console.log('añadiendo', beaconObject);
                          reference.add(beaconObject.beacon).then(data => {
                          }).catch(error => {
                          });
                        });
                      },
                      (error: any) => console.log(error)
                    );
                  }
                })
                .catch()
            }
          }

        });
      }
    });
  }

  listenToWifiBeaconEvents() {
    let options: GyroscopeOptions = {
      frequency: 100
    };
    if (this.wifiInterval) {
      clearInterval(this.wifiInterval);
    }
    this.wifiInterval = setInterval(() => {
      this.wifiProvider.getWifiBeacons().then((networks: Array<HotspotNetwork>) => {
        if (this.platform.is('cordova')) {
          let gyroscope: any = {};
          let accelerometer: any = {};
          if (this.gyroscope) {
            this.gyroscope.getCurrent(options)
              .then((orientation: GyroscopeOrientation) => {
                gyroscope.x = orientation.x;
                gyroscope.y = orientation.y;
                gyroscope.z = orientation.z;
                if (this.deviceMotion) {
                  // Get the device current acceleration
                  this.deviceMotion.getCurrentAcceleration().then(
                    (acceleration: DeviceMotionAccelerationData) => {
                      //console.log(acceleration);
                      accelerometer.X = acceleration.x;
                      accelerometer.Y = acceleration.y;
                      accelerometer.Z = acceleration.z;
                      networks.forEach((network) => {
                        let reference = this.afs.collection('beacons').doc('wifi').collection('beacons');
                        network['accelerometer'] = accelerometer;
                        network['gyroscope'] = gyroscope;
                        network['time'] = new Date().getTime();
                        console.log('añadiendo wifi', network);
                        reference.add(network);
                      });
                    },
                    (error: any) => console.log(error)
                  );
                }
              })
              .catch()
          }
        }
        this.networks = networks;
      });
    }, this.beaconTime);
  }

  openConfig() {
    this.navCtrl.push(BeaconConfigMenuPage);
    return;
  }

  ionViewWillLeave() {
    this.events.unsubscribe('didRangeBeaconsInRegion');
    clearInterval(this.wifiInterval);
    return;
  }

}