import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MapDrawerPage } from '../map/mapDrawer'
import { ImuPage } from '../imu/imu';
import { BeaconsPage } from '../beacons/beacons';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';

/**
 *
 * @author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private authProvider: AuthProvider) {

  }

  openMap() {
    this.navCtrl.push(MapDrawerPage);
  }

  openImu() {
    this.navCtrl.push(ImuPage);
  }

  openBeacons() {
    this.navCtrl.push(BeaconsPage);
  }

  async logOut() {
    await this.authProvider.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  async ionViewWillEnter() {
    let isLoggedIn = await this.authProvider.isLoggedIn();
    if (!isLoggedIn) {
      this.navCtrl.setRoot(LoginPage);
    }
    return;
  }

}
