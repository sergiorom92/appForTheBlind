package com.usa.appForBlind;

import android.app.ListActivity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.ListView;

import com.usa.appForBlind.DB.DatabaseManager;
import com.usa.appForBlind.DB.FirebaseDatabaseManagerImp;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
public class BeaconActivity extends ListActivity implements BeaconConsumer {

    public static final String TAG = "BeaconsEverywhere";
    private BeaconManager beaconManager;
    private ListView lv;
    Map<String, String> your_array_list;
    MyAdapter arrayAdapter;
    DatabaseManager myDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);

        your_array_list = new HashMap<>();
        arrayAdapter = new MyAdapter(your_array_list);
        setListAdapter(arrayAdapter);

        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser()
                .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.setForegroundScanPeriod(3000);
        beaconManager.bind(this);
        this.myDataBase = new FirebaseDatabaseManagerImp();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        final Region region = new Region("myBeacons", Identifier.parse("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);

        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                try {
                    Log.d(TAG, "didEnterRegion");
                    beaconManager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didExitRegion(Region region) {
                try {
                    Log.d(TAG, "didExitRegion");
                    beaconManager.stopRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {

            }
        });


        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                for (Beacon oneBeacon : beacons) {

                    your_array_list.put(oneBeacon.getId1() + "/" + oneBeacon.getId2() + "/" + oneBeacon.getId3(), oneBeacon.toString() + " Distancia: " + oneBeacon.getDistance() + " Mac: " + oneBeacon.getBluetoothAddress() + " Nombre: " + oneBeacon.getBluetoothName());
                    Log.d(TAG, "distance : " + oneBeacon.getDistance() + " id:" + oneBeacon.getId1() + "/" + oneBeacon.getId2() + "/" + oneBeacon.getId3());
                    String hora = System.currentTimeMillis() + "";
                    myDataBase.insertBeaconRecord("beacons", oneBeacon);

                    //DatabaseReference referencia = mDatabase.child("beacons").child(oneBeacon.getBluetoothAddress()).child(hora);

                    //referencia.child("uuid").setValue(oneBeacon.getId1() + "/" + oneBeacon.getId2() + "/" + oneBeacon.getId3());
                    //referencia.child("distancia").setValue(oneBeacon.getDistance());
                    //referencia.child("tipo").setValue(oneBeacon.getBeaconTypeCode());
                    //referencia.child("potencia").setValue(oneBeacon.getTxPower());


                    runOnUiThread(new Runnable() {
                        public void run() {
                            arrayAdapter = new MyAdapter(your_array_list);
                            setListAdapter(arrayAdapter);
                            arrayAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        try {
            beaconManager.startMonitoringBeaconsInRegion(region);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

}
