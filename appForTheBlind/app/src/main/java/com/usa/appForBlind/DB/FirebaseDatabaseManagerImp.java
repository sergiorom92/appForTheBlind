package com.usa.appForBlind.DB;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.altbeacon.beacon.Beacon;

/**
 * Author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
public class FirebaseDatabaseManagerImp implements  DatabaseManager {

    private DatabaseReference mDatabase;

    @Override
    public void insertBeaconRecord(String path, Beacon record) {
        String hora = System.currentTimeMillis() + "";
        Log.d("Intert DB" , record.getBluetoothAddress());
        DatabaseReference firebaseReference = mDatabase.child(path).child(record.getBluetoothAddress()).child(hora);
        firebaseReference.child("uuid").setValue(record.getId1() + "/" + record.getId2() + "/" + record.getId3());
        firebaseReference.child("distancia").setValue(record.getDistance());
        firebaseReference.child("tipo").setValue(record.getBeaconTypeCode());
        firebaseReference.child("potencia").setValue(record.getTxPower());
    }

    @Override
    public Beacon getBeaconRecord(String path) {
        return null;
    }

    public FirebaseDatabaseManagerImp(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

}
