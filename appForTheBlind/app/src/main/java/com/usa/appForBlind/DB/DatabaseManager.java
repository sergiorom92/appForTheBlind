package com.usa.appForBlind.DB;
import org.altbeacon.beacon.Beacon;

/**
 * Author Sergio David Romero Maldonado <sergiorom92@gmail.com>
 */
public interface DatabaseManager {
    void insertBeaconRecord(String path, Beacon record);
    Beacon getBeaconRecord(String path);
}
